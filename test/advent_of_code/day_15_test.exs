defmodule AdventOfCode.Day15Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day15

  test "part1" do
    assert 436 == part1("0,3,6")
    assert 1 == part1("1,3,2")
    assert 10 == part1("2,1,3")
    assert 27 == part1("1,2,3")
    assert 78 == part1("2,3,1")
    assert 438 == part1("3,2,1")
    assert 1836 == part1("3,1,2")
  end

  @tag timeout: :infinity
  @tag :slow
  test "part2 1" do
    assert 175_594 == part2("0,3,6")
    assert 2578 == part2("1,3,2")
    assert 3_544_142 == part2("2,1,3")
    assert 261_214 == part2("1,2,3")
    assert 6_895_259 == part2("2,3,1")
    assert 18 == part2("3,2,1")
    assert 362 == part2("3,1,2")
  end
end
