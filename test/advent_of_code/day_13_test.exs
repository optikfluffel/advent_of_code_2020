defmodule AdventOfCode.Day13Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day13

  test "part1" do
    input = """
    939
    7,13,x,x,59,x,31,19
    """

    assert 295 == part1(input)
  end

  test "part2" do
    assert 3417 == part2("17,x,13,19")
    assert 754_018 == part2("67,7,59,61")
    assert 779_210 == part2("67,x,7,59,61")

    assert 1_068_781 == part2("7,13,x,x,59,x,31,19")

    assert 1_261_476 == part2("67,7,x,59,61")
    assert 1_202_161_486 == part2("1789,37,47,1889")
  end
end
