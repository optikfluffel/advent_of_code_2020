defmodule AdventOfCode.Day09Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day09

  @input """
         35
         20
         15
         25
         47
         40
         62
         55
         65
         95
         102
         117
         150
         182
         127
         219
         299
         277
         309
         576
         """
         |> String.split("\n", trim: true)

  test "part1" do
    assert 127 == part1(@input, 5)
  end

  test "part2" do
    assert 62 == part2(@input, 5)
  end
end
