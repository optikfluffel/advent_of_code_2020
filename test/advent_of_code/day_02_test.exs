defmodule AdventOfCode.Day02Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day02

  @input [
    "1-3 a: abcde",
    "1-3 b: cdefg",
    "2-9 c: ccccccccc"
  ]

  test "part1" do
    assert 2 == part1(@input)
  end

  test "part2" do
    assert 1 == part2(@input)
  end
end
