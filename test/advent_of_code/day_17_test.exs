defmodule AdventOfCode.Day17Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day17

  @input """
  .#.
  ..#
  ###
  """

  test "part1" do
    assert 112 == part1(@input)
  end

  @tag :slow
  test "part2" do
    assert 848 == part2(@input)
  end
end
