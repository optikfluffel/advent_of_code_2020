defmodule AdventOfCode.Day08Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day08

  @input """
         nop +0
         acc +1
         jmp +4
         acc +3
         jmp -3
         acc -99
         acc +1
         jmp -4
         acc +6
         """
         |> String.split("\n", trim: true)

  test "part1" do
    assert 5 == part1(@input)
  end

  test "part2" do
    assert 8 == part2(@input)
  end
end
