defmodule AdventOfCode.Day03Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day03

  @input """
         ..##.......
         #...#...#..
         .#....#..#.
         ..#.#...#.#
         .#...##..#.
         ..#.##.....
         .#.#.#....#
         .#........#
         #.##...#...
         #...##....#
         .#..#...#.#
         """
         |> String.split("\n", trim: true)

  test "part1" do
    assert 7 == part1(@input)
  end

  test "part2" do
    assert 336 == part2(@input)
  end
end
