defmodule AdventOfCode.Day14Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day14

  test "part1" do
    input =
      """
      mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
      mem[8] = 11
      mem[7] = 101
      mem[8] = 0
      """
      |> String.split("\n", trim: true)

    assert 165 == part1(input)
  end

  test "part2" do
    input =
      """
      mask = 000000000000000000000000000000X1001X
      mem[42] = 100
      mask = 00000000000000000000000000000000X0XX
      mem[26] = 1
      """
      |> String.split("\n", trim: true)

    assert 208 == part2(input)
  end
end
