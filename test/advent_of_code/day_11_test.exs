defmodule AdventOfCode.Day11Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day11

  @input """
         L.LL.LL.LL
         LLLLLLL.LL
         L.L.L..L..
         LLLL.LL.LL
         L.LL.LL.LL
         L.LLLLL.LL
         ..L.L.....
         LLLLLLLLLL
         L.LLLLLL.L
         L.LLLLL.LL
         """
         |> String.split("\n", trim: true)

  test "part1" do
    assert 37 == part1(@input)
  end

  test "part2" do
    assert 26 == part2(@input)
  end
end
