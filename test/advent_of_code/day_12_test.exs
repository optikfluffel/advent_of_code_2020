defmodule AdventOfCode.Day12Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day12

  @input """
         F10
         N3
         F7
         R90
         F11
         """
         |> String.split("\n", trim: true)

  test "part1" do
    assert 25 == part1(@input)
  end

  test "part2" do
    assert 286 == part2(@input)
  end
end
