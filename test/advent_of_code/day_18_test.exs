defmodule AdventOfCode.Day18Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day18

  test "part1" do
    assert 71 == part1("1 + 2 * 3 + 4 * 5 + 6")
    assert 51 == part1("1 + (2 * 3) + (4 * (5 + 6))")
    assert 26 == part1("2 * 3 + (4 * 5)")
    assert 437 == part1("5 + (8 * 3 + 9 + 3 * 4 * 3)")
    assert 12240 == part1("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))")
    assert 13632 == part1("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")
  end

  @tag :skip
  test "part2" do
    input = nil
    result = part2(input)

    assert result
  end
end
