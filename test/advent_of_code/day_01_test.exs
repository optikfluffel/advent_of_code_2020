defmodule AdventOfCode.Day01Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day01

  @input [1721, 979, 366, 299, 675, 1456]

  test "part1" do
    assert 514_579 == part1(@input)
  end

  test "part2" do
    assert 241_861_950 == part2(@input)
  end
end
