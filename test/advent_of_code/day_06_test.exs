defmodule AdventOfCode.Day06Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day06

  @input """
  abc

  a
  b
  c

  ab
  ac

  a
  a
  a
  a

  b
  """
  test "part1" do
    assert 11 == part1(@input)
  end

  test "part2" do
    assert 6 == part2(@input)
  end
end
