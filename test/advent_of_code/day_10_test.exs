defmodule AdventOfCode.Day10Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day10

  @input """
         16
         10
         15
         5
         1
         11
         7
         19
         6
         12
         4
         """
         |> String.split("\n", trim: true)

  @larger_input """
                28
                33
                18
                42
                31
                14
                46
                20
                48
                47
                24
                23
                49
                45
                19
                38
                39
                11
                1
                32
                25
                35
                8
                17
                7
                9
                4
                2
                34
                10
                3
                """
                |> String.split("\n", trim: true)

  test "part1" do
    assert 35 == part1(@input)
  end

  test "part1 larger example" do
    assert 220 == part1(@larger_input)
  end

  test "part2" do
    assert 8 == part2(@input)
  end

  test "part2 larger example" do
    assert 19208 == part2(@larger_input)
  end
end
