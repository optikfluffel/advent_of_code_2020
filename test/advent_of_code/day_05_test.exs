defmodule AdventOfCode.Day05Test do
  use ExUnit.Case, async: true

  import AdventOfCode.Day05

  test "part1" do
    assert 357 == part1(["FBFBBFFRLR"])
    assert 567 == part1(["BFFFBBFRRR"])
    assert 119 == part1(["FFFBBBFRRR"])
    assert 820 == part1(["BBFFBBFRLL"])

    input =
      """
      FBFBBFFRLR
      BFFFBBFRRR
      FFFBBBFRRR
      BBFFBBFRLL
      """
      |> String.split("\n", trim: true)

    assert 820 == part1(input)
  end

  @tag :skip
  test "part2" do
    input = nil
    result = part2(input)

    assert result
  end
end
