defmodule AdventOfCode.Day15 do
  def part1(args) do
    {as_map, last, current_turn} = parse_input(args)
    play(as_map, {last, current_turn}, 2020)
  end

  def part2(args) do
    {as_map, last, current_turn} = parse_input(args)
    play(as_map, {last, current_turn}, 30_000_000)
  end

  defp parse_input(args) do
    with_index =
      args
      |> String.split(",")
      |> Enum.map(&String.to_integer/1)
      |> Enum.with_index(1)

    as_map = Enum.reduce(with_index, %{}, &next_acc/2)

    last = with_index |> List.last() |> elem(0)
    current_turn = length(with_index) + 1

    {as_map, last, current_turn}
  end

  defp play(acc, {last, stop}, stop), do: current_number(acc, last)

  defp play(acc, {last, current_turn}, stop) do
    number = current_number(acc, last)
    next_acc = next_acc({number, current_turn}, acc)

    play(next_acc, {number, current_turn + 1}, stop)
  end

  defp next_acc({n, current_turn}, acc) do
    case Map.get(acc, n) do
      nil -> Map.put(acc, n, {current_turn, nil})
      {a, _b} -> Map.put(acc, n, {current_turn, a})
    end
  end

  defp current_number(acc, last_number) do
    case Map.get(acc, last_number) do
      {_turn, nil} -> 0
      {turn_a, turn_b} -> turn_a - turn_b
    end
  end
end
