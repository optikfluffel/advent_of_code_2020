defmodule AdventOfCode.Day09 do
  def part1(args, preamble_size \\ 25) do
    args_as_numbers = Enum.map(args, &String.to_integer/1)

    preamble = Enum.take(args_as_numbers, preamble_size)
    numbers = Enum.drop(args_as_numbers, preamble_size)

    find_first_non_matching(preamble, numbers)
  end

  def part2(args, preamble_size \\ 25) do
    args_as_numbers = Enum.map(args, &String.to_integer/1)
    desired_number = part1(args, preamble_size)
    {:ok, contiguous_set} = find_contiguous_set(args_as_numbers, desired_number)

    Enum.min(contiguous_set) + Enum.max(contiguous_set)
  end

  defp find_first_non_matching(_preamble, []), do: {:error, :not_found}

  defp find_first_non_matching(preamble, [current_number | rest]) do
    case AdventOfCode.Day01.find_two_summands(preamble, current_number) do
      {:ok, {_x, _y}} ->
        next_preamble = Enum.drop(preamble, 1) ++ [current_number]
        find_first_non_matching(next_preamble, rest)

      {:error, :not_found} ->
        current_number
    end
  end

  defp find_contiguous_set([], _number), do: {:error, :not_found}

  defp find_contiguous_set([_x | rest] = list, number) do
    case collect_until_too_big(list, number, []) do
      {:ok, set} -> {:ok, set}
      {:error, :not_found} -> find_contiguous_set(rest, number)
    end
  end

  defp collect_until_too_big([], _n, _collected), do: {:error, :not_found}

  defp collect_until_too_big([x | rest], n, collected) do
    maybe_next_collected = [x | collected]
    sum = Enum.sum(maybe_next_collected)

    cond do
      sum == n -> {:ok, Enum.reverse(maybe_next_collected)}
      sum < n -> collect_until_too_big(rest, n, maybe_next_collected)
      sum > n -> {:error, :not_found}
    end
  end
end
