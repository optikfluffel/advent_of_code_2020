defmodule AdventOfCode.Day11 do
  def part1(args) do
    args
    |> rows_to_grid(0, %{})
    |> step_through(%{})
    |> Enum.count(fn {_coord, state} -> is_occupied?(state) end)
  end

  def part2(args) do
    args
    |> rows_to_grid(0, %{})
    |> step_through_v2(%{})
    |> Enum.count(fn {_coord, state} -> is_occupied?(state) end)
  end

  defp rows_to_grid([], _row_number, grid), do: grid

  defp rows_to_grid([current | rest], row_number, grid) do
    next_grid =
      current
      |> String.graphemes()
      |> Enum.with_index()
      |> Enum.reduce(grid, fn
        {"L", i}, acc -> Map.put_new(acc, {row_number, i}, :empty)
        {".", i}, acc -> Map.put_new(acc, {row_number, i}, :floor)
      end)

    rows_to_grid(rest, row_number + 1, next_grid)
  end

  defp step_through(final_grid, final_grid), do: final_grid

  defp step_through(grid, _previous) do
    grid
    |> Enum.map(&step_single_cell(&1, grid))
    |> Enum.into(%{})
    |> step_through(grid)
  end

  defp step_through_v2(final_grid, final_grid), do: final_grid

  defp step_through_v2(grid, _previous) do
    grid
    |> Enum.map(&step_single_cell_v2(&1, grid))
    |> Enum.into(%{})
    |> step_through_v2(grid)
  end

  defp step_single_cell({coord, :occupied} = cell, grid) do
    case coord |> neighbor_states(grid) |> Enum.count(&is_occupied?/1) >= 4 do
      true -> {coord, :empty}
      false -> cell
    end
  end

  defp step_single_cell({coord, :empty} = cell, grid) do
    case coord |> neighbor_states(grid) |> Enum.all?(&(not is_occupied?(&1))) do
      true -> {coord, :occupied}
      false -> cell
    end
  end

  defp step_single_cell({_coord, :floor} = cell, _grid), do: cell

  defp step_single_cell_v2({coord, :occupied} = cell, grid) do
    case coord |> neighbor_states_v2(grid) |> Enum.count(&is_occupied?/1) >= 5 do
      true -> {coord, :empty}
      false -> cell
    end
  end

  defp step_single_cell_v2({coord, :empty} = cell, grid) do
    case coord |> neighbor_states_v2(grid) |> Enum.all?(&(not is_occupied?(&1))) do
      true -> {coord, :occupied}
      false -> cell
    end
  end

  defp step_single_cell_v2({_coord, :floor} = cell, _grid), do: cell

  defp neighbor_states(coords, grid) do
    ~w(top_left top top_right left right bottom_left bottom bottom_right)a
    |> Enum.map(&neighbor_coords(&1, coords))
    |> Enum.map(&Map.get(grid, &1))
    |> Enum.filter(fn state -> state != nil end)
  end

  defp neighbor_states_v2(coords, grid) do
    ~w(top_left top top_right left right bottom_left bottom bottom_right)a
    |> Enum.map(&get_neighbor(coords, grid, &1))
    |> Enum.filter(fn state -> state != nil end)
  end

  defp get_neighbor(coords, grid, direction) do
    maybe_neighbor_coords = neighbor_coords(direction, coords)

    case Map.get(grid, maybe_neighbor_coords) do
      :floor -> get_neighbor(maybe_neighbor_coords, grid, direction)
      neighbor -> neighbor
    end
  end

  defp neighbor_coords(direction, {x, y}) do
    case direction do
      :top_left -> {x - 1, y - 1}
      :top -> {x, y - 1}
      :top_right -> {x + 1, y - 1}
      :left -> {x - 1, y}
      :right -> {x + 1, y}
      :bottom_left -> {x - 1, y + 1}
      :bottom -> {x, y + 1}
      :bottom_right -> {x + 1, y + 1}
    end
  end

  defp is_occupied?(:occupied), do: true
  defp is_occupied?(_state), do: false
end
