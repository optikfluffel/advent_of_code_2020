defmodule AdventOfCode.Day20 do
  defmodule Tile do
    defstruct [:id, :top, :bottom, :left, :right]
  end

  def part1(args) do
    tiles = args |> String.split("\n\n", trim: true) |> Enum.map(&parse_tile/1)

    tiles
    |> find_corners()
    |> Enum.map(&Map.get(&1, :id))
    |> Enum.reduce(&*/2)
  end

  def part2(_args) do
  end

  defp parse_tile(tile_str) do
    ["Tile " <> id_str, content_str] = String.split(tile_str, ":\n", parts: 2)
    content_lines = String.split(content_str, "\n", trim: true)

    top = List.first(content_lines)
    bottom = List.last(content_lines)
    left = content_lines |> Enum.map(&String.first/1) |> Enum.join()
    right = content_lines |> Enum.map(&String.last/1) |> Enum.join()

    %Tile{
      id: String.to_integer(id_str),
      top: top,
      bottom: bottom,
      left: left,
      right: right
    }
  end

  defp find_corners(tiles) do
    all_sides_with_id =
      Enum.map(tiles, fn %Tile{id: id, top: t, bottom: b, left: l, right: r} ->
        sides = [t, b, l, r]
        reversed = Enum.map(sides, &String.reverse/1)
        {id, sides ++ reversed}
      end)

    Enum.filter(tiles, &has_two_unique_sides?(&1, all_sides_with_id))
  end

  defp has_two_unique_sides?(%Tile{id: id, top: t, bottom: b, left: l, right: r}, all_sides) do
    other_sides =
      all_sides
      |> Enum.filter(fn {i, _sides} -> i != id end)
      |> Enum.flat_map(&elem(&1, 1))

    Enum.count([t, b, l, r], fn side -> not Enum.member?(other_sides, side) end) == 2
  end
end
