defmodule AdventOfCode.Day03 do
  @open "."
  @tree "#"

  def part1(rows) do
    count_tree_hits(rows, {3, 1}, 0, 0)
  end

  def part2(rows) do
    shifts = [
      {1, 1},
      {3, 1},
      {5, 1},
      {7, 1},
      {1, 2}
    ]

    shifts |> Enum.map(&count_tree_hits(rows, &1, 0, 0)) |> Enum.reduce(&*/2)
  end

  defp count_tree_hits([], _shift, _current_x, hit_count), do: hit_count

  defp count_tree_hits([row | rest], {x_shift, y_shift} = shift, current_x, hit_count) do
    # - 1 because the [row | rest] pattern match already removes the first row
    next_rows = Enum.drop(rest, y_shift - 1)
    next_x = current_x + x_shift

    position = rem(current_x, String.length(row))

    case String.at(row, position) do
      @open -> count_tree_hits(next_rows, shift, next_x, hit_count)
      @tree -> count_tree_hits(next_rows, shift, next_x, hit_count + 1)
    end
  end
end
