defmodule AdventOfCode.Day06 do
  def part1(args) do
    args
    |> String.trim_trailing("\n")
    |> String.split("\n\n")
    |> Enum.map(&unique_answers/1)
    |> Enum.map(&Enum.count/1)
    |> Enum.sum()
  end

  def part2(args) do
    args
    |> String.trim_trailing("\n")
    |> String.split("\n\n")
    |> Enum.map(&agreeing_answers/1)
    |> Enum.map(&Enum.count/1)
    |> Enum.sum()
  end

  defp unique_answers(group) do
    group |> String.replace("\n", "") |> String.graphemes() |> Enum.uniq()
  end

  defp agreeing_answers(group) do
    group_as_list = String.split(group, "\n")

    group
    |> unique_answers()
    |> Enum.filter(fn a -> Enum.all?(group_as_list, &String.contains?(&1, a)) end)
  end
end
