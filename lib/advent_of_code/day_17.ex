defmodule AdventOfCode.Day17 do
  def part1(args) do
    args
    |> String.split("\n", trim: true)
    |> Enum.with_index()
    |> Enum.flat_map(&parse_line/1)
    |> Enum.into(%{})
    |> run(6)
    |> Enum.count(fn {_coords, state} -> state == :active end)
  end

  def part2(args) do
    args
    |> String.split("\n", trim: true)
    |> Enum.with_index()
    |> Enum.flat_map(&hyper_parse_line/1)
    |> Enum.into(%{})
    |> hyper_run(6)
    |> Enum.count(fn {_coords, state} -> state == :active end)
  end

  defp parse_line({line, y}) do
    line
    |> String.graphemes()
    |> Enum.with_index()
    |> Enum.map(fn
      {"#", x} -> {{x, y, 0}, :active}
      {".", x} -> {{x, y, 0}, :inactive}
    end)
  end

  defp hyper_parse_line({line, y}) do
    line
    |> String.graphemes()
    |> Enum.with_index()
    |> Enum.map(fn
      {"#", x} -> {{x, y, 0, 0}, :active}
      {".", x} -> {{x, y, 0, 0}, :inactive}
    end)
  end

  defp run(cubes, 0), do: cubes

  defp run(cubes, remaining_cycles) do
    cubes_expanded =
      cubes
      |> Enum.map(&elem(&1, 0))
      |> Enum.flat_map(&get_neighbors/1)
      |> Enum.reduce(cubes, fn coords, acc -> Map.put_new(acc, coords, :inactive) end)

    next_cubes =
      cubes_expanded
      |> Enum.map(&update_single_cube(&1, cubes_expanded))
      |> Enum.into(%{})

    run(next_cubes, remaining_cycles - 1)
  end

  defp hyper_run(cubes, 0), do: cubes

  defp hyper_run(cubes, remaining_cycles) do
    cubes_expanded =
      cubes
      |> Enum.map(&elem(&1, 0))
      |> Enum.flat_map(&hyper_get_neighbors/1)
      |> Enum.reduce(cubes, fn coords, acc -> Map.put_new(acc, coords, :inactive) end)

    next_cubes =
      cubes_expanded
      |> Enum.map(&hyper_update_single_cube(&1, cubes_expanded))
      |> Enum.into(%{})

    hyper_run(next_cubes, remaining_cycles - 1)
  end

  defp update_single_cube({coords, state}, cubes) do
    active_count = coords |> get_neighbors() |> count_active_neighbors(cubes)
    new_state = next_state(state, active_count)

    {coords, new_state}
  end

  defp hyper_update_single_cube({coords, state}, cubes) do
    active_count = coords |> hyper_get_neighbors() |> count_active_neighbors(cubes)
    new_state = next_state(state, active_count)

    {coords, new_state}
  end

  defp count_active_neighbors(neighbors, cubes) do
    neighbors
    |> Enum.map(&Map.get(cubes, &1))
    |> Enum.count(fn state -> state == :active end)
  end

  defp next_state(:active, 2), do: :active
  defp next_state(:active, 3), do: :active
  defp next_state(:inactive, 3), do: :active
  defp next_state(:active, _count), do: :inactive
  defp next_state(:inactive, _count), do: :inactive

  defp get_neighbors({x, y, z}) do
    neighbors_incl_self =
      for i <- -1..1 do
        for j <- -1..1 do
          for k <- -1..1 do
            {x + i, y + j, z + k}
          end
        end
      end

    neighbors_incl_self
    |> List.flatten()
    |> Enum.filter(fn coords -> coords != {x, y, z} end)
  end

  defp hyper_get_neighbors({x, y, z, w}) do
    neighbors_incl_self =
      for i <- -1..1 do
        for j <- -1..1 do
          for k <- -1..1 do
            for l <- -1..1 do
              {x + i, y + j, z + k, w + l}
            end
          end
        end
      end

    neighbors_incl_self
    |> List.flatten()
    |> Enum.filter(fn coords -> coords != {x, y, z, w} end)
  end
end
