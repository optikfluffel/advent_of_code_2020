defmodule AdventOfCode.Day01 do
  def part1(args) do
    {:ok, {x, y}} = find_two_summands(args, 2020)
    x * y
  end

  def part2(args) do
    {:ok, {x, y, z}} = find_three_summands(args, 2020)
    x * y * z
  end

  def find_two_summands([], _sum), do: {:error, :not_found}

  def find_two_summands([x | rest], sum) do
    maybe_y = sum - x

    if Enum.member?(rest, maybe_y) do
      {:ok, {x, maybe_y}}
    else
      find_two_summands(rest, sum)
    end
  end

  defp find_three_summands([], _sum), do: {:error, :not_found}

  defp find_three_summands([x | rest], sum) do
    case find_two_summands(rest, sum - x) do
      {:ok, {y, z}} -> {:ok, {x, y, z}}
      {:error, :not_found} -> find_three_summands(rest, sum)
    end
  end
end
