defmodule AdventOfCode.Day07 do
  @type color :: String.t()
  @type rule :: {color, [{pos_integer(), color}] | :none}

  def part1(args) do
    rules = Enum.map(args, &parse_rule/1)
    count_valid_bags(["shiny gold"], rules, [])
  end

  def part2(args) do
    rules = Enum.map(args, &parse_rule/1)
    count_containing_bags("shiny gold", rules)
  end

  defp parse_rule(line) do
    %{"bag" => color, "contains" => contains_str} =
      Regex.named_captures(~r/^(?<bag>\w+\s\w+)\sbags\scontain\s(?<contains>.+)\.\Z/, line)

    contains =
      case contains_str do
        "no other bags" -> :none
        _ -> parse_contains(contains_str)
      end

    {color, contains}
  end

  defp parse_contains(contains) do
    contains
    |> String.split(", ")
    |> Enum.map(&Regex.named_captures(~r/^(?<number>\d)\s(?<color>\w+\s\w+)\sbags*/, &1))
    |> Enum.map(fn %{"color" => c, "number" => n} -> {String.to_integer(n), c} end)
  end

  defp count_valid_bags([], _rules, matched), do: length(matched)

  defp count_valid_bags(desired_colors, rules, matched) do
    new_matched =
      rules
      |> Enum.filter(fn
        {_color, :none} -> false
        {_color, contains} -> Enum.any?(contains, fn {_n, color} -> color in desired_colors end)
      end)
      |> Enum.map(fn {color, _rules} -> color end)

    next_matched = Enum.uniq(matched ++ new_matched)
    next_desired_colors = Enum.filter(new_matched, fn c -> c not in matched end)

    count_valid_bags(next_desired_colors, rules, next_matched)
  end

  defp count_containing_bags(color, rules) do
    relevant_rule = Enum.find(rules, fn {c, _contains} -> c == color end)

    case relevant_rule do
      {_color, :none} ->
        0

      {_color, contains} ->
        contains
        |> Enum.map(fn {n, c} -> n + n * count_containing_bags(c, rules) end)
        |> Enum.sum()
    end
  end
end
