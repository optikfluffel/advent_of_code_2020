defmodule AdventOfCode.Day12 do
  @initial_waypoint %{x: 10, y: 1}
  @initial_ship %{direction: :east, x: 0, y: 0, waypoint: @initial_waypoint}

  def part1(args) do
    ship = args |> Enum.map(&parse_line/1) |> Enum.reduce(@initial_ship, &drive_part1/2)

    abs(ship.x) + abs(ship.y)
  end

  def part2(args) do
    ship = args |> Enum.map(&parse_line/1) |> Enum.reduce(@initial_ship, &drive_part2/2)

    abs(ship.x) + abs(ship.y)
  end

  defp parse_line("N" <> amount_str), do: {:north, String.to_integer(amount_str)}
  defp parse_line("S" <> amount_str), do: {:south, String.to_integer(amount_str)}
  defp parse_line("E" <> amount_str), do: {:east, String.to_integer(amount_str)}
  defp parse_line("W" <> amount_str), do: {:west, String.to_integer(amount_str)}
  defp parse_line("L" <> amount_str), do: {:left, String.to_integer(amount_str)}
  defp parse_line("R" <> amount_str), do: {:right, String.to_integer(amount_str)}
  defp parse_line("F" <> amount_str), do: {:forward, String.to_integer(amount_str)}

  defp drive_part1({:north, amount}, ship), do: %{ship | y: ship.y + amount}
  defp drive_part1({:south, amount}, ship), do: %{ship | y: ship.y - amount}
  defp drive_part1({:east, amount}, ship), do: %{ship | x: ship.x + amount}
  defp drive_part1({:west, amount}, ship), do: %{ship | x: ship.x - amount}
  defp drive_part1({:left, amount}, ship), do: turn(ship, :left, amount)
  defp drive_part1({:right, amount}, ship), do: turn(ship, :right, amount)
  defp drive_part1({:forward, amount}, ship), do: drive_part1({ship.direction, amount}, ship)

  defp drive_part2({direction, amount}, ship) when direction in ~w(north south east west)a do
    new_waypoint = drive_part1({direction, amount}, ship.waypoint)
    %{ship | waypoint: new_waypoint}
  end

  defp drive_part2({direction, amount}, ship) when direction in ~w(left right)a do
    new_waypoint = turn_waypoint(ship.waypoint, {direction, amount})
    %{ship | waypoint: new_waypoint}
  end

  defp drive_part2({:forward, amount}, %{waypoint: waypoint} = ship) do
    new_x = ship.x + amount * waypoint.x
    new_y = ship.y + amount * waypoint.y

    %{ship | x: new_x, y: new_y}
  end

  defp turn(ship, turn_direction, amount) do
    new_direction = compass(ship.direction, turn_direction, amount)
    %{ship | direction: new_direction}
  end

  defp turn_waypoint(%{x: x, y: y}, {_direction, 180}) do
    %{x: x * -1, y: y * -1}
  end

  defp turn_waypoint(%{x: x, y: y}, instruction)
       when instruction in [{:left, 90}, {:right, 270}] do
    %{x: y * -1, y: x}
  end

  defp turn_waypoint(%{x: x, y: y}, instruction)
       when instruction in [{:left, 270}, {:right, 90}] do
    %{x: y, y: x * -1}
  end

  defp compass(:north, _direction, 180), do: :south
  defp compass(:north, :left, 90), do: :west
  defp compass(:north, :right, 270), do: :west
  defp compass(:north, :left, 270), do: :east
  defp compass(:north, :right, 90), do: :east

  defp compass(:south, _direction, 180), do: :north
  defp compass(:south, :left, 90), do: :east
  defp compass(:south, :right, 270), do: :east
  defp compass(:south, :left, 270), do: :west
  defp compass(:south, :right, 90), do: :west

  defp compass(:east, _direction, 180), do: :west
  defp compass(:east, :left, 90), do: :north
  defp compass(:east, :right, 270), do: :north
  defp compass(:east, :left, 270), do: :south
  defp compass(:east, :right, 90), do: :south

  defp compass(:west, _direction, 180), do: :east
  defp compass(:west, :left, 90), do: :south
  defp compass(:west, :right, 270), do: :south
  defp compass(:west, :left, 270), do: :north
  defp compass(:west, :right, 90), do: :north
end
