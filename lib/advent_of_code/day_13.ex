defmodule AdventOfCode.Day13 do
  def part1(args) do
    [timestamp_str, busses_str] = String.split(args, "\n", trim: true)

    timestamp = String.to_integer(timestamp_str)

    busses =
      busses_str
      |> String.split(",")
      |> Enum.filter(&(&1 != "x"))
      |> Enum.map(&String.to_integer/1)

    {departure, bus} = find_earliest_departure(timestamp, busses)

    (departure - timestamp) * bus
  end

  def part2(args) do
    [{first_bus, _drift} | _] =
      busses =
      args
      |> String.split(",")
      |> Enum.reduce({[], 0}, fn
        "x", {acc, drift} -> {acc, drift + 1}
        n_str, {acc, drift} -> {[{String.to_integer(n_str), drift} | acc], drift + 1}
      end)
      |> elem(0)
      |> Enum.reverse()

    find_earliest_timestamp(busses, [], 0, first_bus)
  end

  defp find_earliest_departure(timestamp, busses) do
    case Enum.find(busses, fn bus -> rem(timestamp, bus) == 0 end) do
      nil -> find_earliest_departure(timestamp + 1, busses)
      bus -> {timestamp, bus}
    end
  end

  # last
  defp find_earliest_timestamp([], found, timestamp, step_size) do
    case Enum.all?(found, fn {n, drift} -> rem(timestamp + drift, n) == 0 end) do
      false -> find_earliest_timestamp([], found, timestamp + step_size, step_size)
      true -> timestamp
    end
  end

  # first
  defp find_earliest_timestamp([first | [second | rest]] = busses, [], timestamp, step_size) do
    case Enum.all?([first, second], fn {n, drift} -> rem(timestamp + drift, n) == 0 end) do
      false ->
        find_earliest_timestamp(busses, [], timestamp + step_size, step_size)

      true ->
        next_step_size = lcm(step_size, elem(second, 0))
        find_earliest_timestamp(rest, [first, second], timestamp, next_step_size)
    end
  end

  defp find_earliest_timestamp([current | rest] = busses, found, timestamp, step_size) do
    case Enum.all?([current | found], fn {n, drift} -> rem(timestamp + drift, n) == 0 end) do
      false ->
        find_earliest_timestamp(busses, found, timestamp + step_size, step_size)

      true ->
        next_step_size = lcm(step_size, elem(current, 0))
        find_earliest_timestamp(rest, [current | found], timestamp, next_step_size)
    end
  end

  defp gcd(a, 0), do: abs(a)
  defp gcd(a, b), do: gcd(b, rem(a, b))

  def lcm(a, b), do: div(abs(a * b), gcd(a, b))
end
