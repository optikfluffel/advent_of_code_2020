defmodule AdventOfCode.Day05 do
  def part1(args) do
    args |> Stream.map(&parse/1) |> Stream.map(&seat_id/1) |> Enum.max()
  end

  def part2(args) do
    {:ok, my_seat} =
      args
      |> Stream.map(&parse/1)
      |> Stream.map(&seat_id/1)
      |> Enum.sort()
      |> find_missing()

    my_seat
  end

  defp parse(boarding_pass) do
    {x, y} = String.split_at(boarding_pass, 7)

    row = row(x, <<>>)
    column = column(y, <<>>)

    {row, column}
  end

  defp row("", result), do: :binary.decode_unsigned(<<0::1, result::bitstring>>)
  defp row("F" <> rest, result), do: row(rest, <<result::bitstring, 0::1>>)
  defp row("B" <> rest, result), do: row(rest, <<result::bitstring, 1::1>>)

  defp column("", result), do: :binary.decode_unsigned(<<0::5, result::bitstring>>)
  defp column("L" <> rest, result), do: column(rest, <<result::bitstring, 0::1>>)
  defp column("R" <> rest, result), do: column(rest, <<result::bitstring, 1::1>>)

  defp seat_id({row, column}), do: row * 8 + column

  defp find_missing([]), do: {:error, :not_found}

  defp find_missing([current | rest]) do
    next = current + 1

    case rest do
      [^next | _] -> find_missing(rest)
      _ -> {:ok, next}
    end
  end
end
