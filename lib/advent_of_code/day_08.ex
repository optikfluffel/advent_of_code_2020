defmodule AdventOfCode.Day08 do
  def part1(args) do
    {:error, {:infinite_loop, acc}} =
      args
      |> Enum.with_index()
      |> Enum.map(&parse_operations/1)
      |> Enum.into(%{})
      |> run(0, 0)

    acc
  end

  def part2(args) do
    ops_list =
      args
      |> Enum.with_index()
      |> Enum.map(&parse_operations/1)

    ops_map = Enum.into(ops_list, %{})

    {:ok, acc} = search_fix(ops_list, ops_map)

    acc
  end

  defp parse_operations({line, index}) do
    [operation_str, value_str] = String.split(line, " ", parts: 2)

    operation =
      case operation_str do
        "acc" -> :acc
        "jmp" -> :jump
        "nop" -> :noop
      end

    value = String.to_integer(value_str)

    {index, {operation, value, false}}
  end

  defp run(ops, pointer, acc) do
    case Map.get(ops, pointer) do
      nil ->
        {:ok, acc}

      {_operation, _value, true} ->
        {:error, {:infinite_loop, acc}}

      {:acc, value, false} ->
        run(%{ops | pointer => {:acc, value, true}}, pointer + 1, acc + value)

      {:jump, value, false} ->
        run(%{ops | pointer => {:jump, value, true}}, pointer + value, acc)

      {:noop, value, false} ->
        run(%{ops | pointer => {:noop, value, true}}, pointer + 1, acc)
    end
  end

  defp search_fix([], _ops_map), do: {:error, :not_found}
  defp search_fix([{_index, {:acc, _value, false}} | rest], ops), do: search_fix(rest, ops)

  defp search_fix([{index, {op, value, false}} | rest], ops) do
    changed_op =
      case op do
        :jump -> :noop
        :noop -> :jump
      end

    maybe_correct_ops = %{ops | index => {changed_op, value, false}}

    case run(maybe_correct_ops, 0, 0) do
      {:ok, acc} -> {:ok, acc}
      {:error, {:infinite_loop, _acc}} -> search_fix(rest, ops)
    end
  end
end
