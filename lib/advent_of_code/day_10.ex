defmodule AdventOfCode.Day10 do
  def part1(args) do
    {ones, threes} =
      args
      |> adapters_from_input()
      |> count_joltage_differences(0, {0, 0})

    ones * threes
  end

  def part2(args) do
    args
    |> adapters_from_input()
    |> collect_joltage_differences(0, [])
    |> count_sequences(%{})
    |> Enum.reduce(1, fn
      {1, _count}, acc -> acc
      {2, count}, acc -> acc * pow(2, count)
      {3, count}, acc -> acc * pow(4, count)
      {s, count}, acc when s >= 4 -> acc * pow(7, count)
    end)
  end

  defp adapters_from_input(args) do
    parsed_adapters = args |> Enum.map(&String.to_integer/1)
    own_adapter = Enum.max(parsed_adapters) + 3

    Enum.sort([own_adapter | parsed_adapters])
  end

  defp count_joltage_differences([], _previous, count), do: count

  defp count_joltage_differences([current | rest], previous, {ones, threes}) do
    case current - previous do
      1 -> count_joltage_differences(rest, current, {ones + 1, threes})
      3 -> count_joltage_differences(rest, current, {ones, threes + 1})
    end
  end

  defp collect_joltage_differences([], _previous, acc), do: acc

  defp collect_joltage_differences([current | rest], previous, acc) do
    collect_joltage_differences(rest, current, [current - previous | acc])
  end

  defp count_sequences([], acc), do: acc

  defp count_sequences([3 | [1 | maybe_rest]], acc) do
    sequence_length =
      Enum.reduce_while(maybe_rest, 1, fn
        1, count -> {:cont, count + 1}
        _, count -> {:halt, count}
      end)

    rest = Enum.drop(maybe_rest, sequence_length - 1)

    next_acc =
      case Map.get(acc, sequence_length) do
        nil -> Map.put(acc, sequence_length, 1)
        x -> Map.put(acc, sequence_length, x + 1)
      end

    count_sequences(rest, next_acc)
  end

  defp count_sequences([3 | rest], acc), do: count_sequences(rest, acc)

  defp pow(x, y), do: round(:math.pow(x, y))
end
