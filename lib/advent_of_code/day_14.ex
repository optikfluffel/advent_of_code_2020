defmodule AdventOfCode.Day14 do
  def part1(args) do
    args
    |> Enum.map(&parse_line/1)
    |> Enum.reduce(%{}, &run_v1/2)
    |> Map.delete(:current_bitmask)
    |> Enum.map(&elem(&1, 1))
    |> Enum.sum()
  end

  def part2(args) do
    args
    |> Enum.map(&parse_line/1)
    |> Enum.reduce(%{}, &run_v2/2)
    |> Map.delete(:current_bitmask)
    |> Enum.map(&elem(&1, 1))
    |> Enum.sum()
  end

  defp parse_line("mask = " <> bitmask_str) do
    bitmask = parse_bitmask_str(bitmask_str, [])

    {:bitmask, bitmask}
  end

  defp parse_line("mem" <> mem_str) do
    %{"address" => address_str, "value" => value_str} =
      Regex.named_captures(~r/^\[(?<address>\d+)\]\s=\s(?<value>\d+)$/, mem_str)

    address = String.to_integer(address_str)
    value = String.to_integer(value_str)

    {:write, {address, value}}
  end

  defp parse_bitmask_str("", acc), do: Enum.reverse(acc)
  defp parse_bitmask_str("0" <> rest, acc), do: parse_bitmask_str(rest, [0 | acc])
  defp parse_bitmask_str("1" <> rest, acc), do: parse_bitmask_str(rest, [1 | acc])
  defp parse_bitmask_str("X" <> rest, acc), do: parse_bitmask_str(rest, [:X | acc])

  defp run_v1({:bitmask, new_bitmask}, result) do
    Map.put(result, :current_bitmask, new_bitmask)
  end

  defp run_v1({:write, {address, value}}, result) do
    correct_value = apply_bitmask_v1(value, result.current_bitmask)

    Map.put(result, address, correct_value)
  end

  defp apply_bitmask_v1(value, bitmask) do
    value
    |> Integer.digits(2)
    |> maybe_fill_up()
    |> Enum.zip(bitmask)
    |> Enum.reduce([], fn
      {original_value, :X}, acc -> [original_value | acc]
      {_original_value, bitmask_value}, acc -> [bitmask_value | acc]
    end)
    |> Enum.reverse()
    |> Integer.undigits(2)
  end

  defp run_v2({:bitmask, new_bitmask}, result) do
    Map.put(result, :current_bitmask, new_bitmask)
  end

  defp run_v2({:write, {address, value}}, result) do
    correct_addresses = apply_bitmask_v2(address, result.current_bitmask)

    Enum.reduce(correct_addresses, result, fn address, acc -> Map.put(acc, address, value) end)
  end

  defp apply_bitmask_v2(address, bitmask) do
    address
    |> Integer.digits(2)
    |> maybe_fill_up()
    |> Enum.zip(bitmask)
    |> Enum.reduce([], fn
      {original_address, 0}, [] -> [[original_address]]
      {_original_address, 1}, [] -> [[1]]
      {_original_address, :X}, [] -> [[0], [1]]
      {original_address, 0}, acc -> Enum.map(acc, fn addr -> [original_address | addr] end)
      {_original_address, 1}, acc -> Enum.map(acc, fn addr -> [1 | addr] end)
      {_original_address, :X}, acc -> Enum.flat_map(acc, fn addr -> [[0 | addr], [1 | addr]] end)
    end)
    |> Enum.map(&Enum.reverse/1)
    |> Enum.map(&Integer.undigits(&1, 2))
  end

  defp maybe_fill_up(digits) when length(digits) < 36 do
    amount = 36 - length(digits)
    missing_zeros = for _ <- 1..amount, do: 0

    missing_zeros ++ digits
  end

  defp maybe_fill_up(digits), do: digits
end
