defmodule AdventOfCode.Day18 do
  import NimbleParsec

  def part1(args) do
    args
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_line/1)
    |> Enum.map(&calculate/1)
    |> Enum.sum()
  end

  def part2(_args) do
  end

  defp parse_line(line) do
    {:ok, [result], "", _, _, _} = line |> String.replace(" ", "") |> my_parser()

    result
  end

  defp calculate(a) when is_integer(a), do: a

  defp calculate({op, [a, b]}) do
    apply(Kernel, op, [calculate(a), calculate(b)])
  end

  # ---------- Parser
  # Special thanks and ❤ to:
  # https://stefan.lapers.be/posts/elixir-writing-an-expression-parser-with-nimble-parsec/

  number = integer(min: 1)

  add = string("+") |> replace(:+) |> label("+")
  multiply = string("*") |> replace(:*) |> label("*")

  l_paren = ascii_char([?(]) |> label("(")
  r_paren = ascii_char([?)]) |> label(")")
  grouping = ignore(l_paren) |> parsec(:my_parser) |> ignore(r_paren)

  defcombinatorp(:term, choice([number, add, multiply, grouping]))

  defparsec(
    :my_parser,
    parsec(:term)
    |> repeat(parsec(:term))
    |> reduce(:fold_infixl)
  )

  defp fold_infixl(acc) do
    acc
    |> Enum.reverse()
    |> Enum.chunk_every(2)
    |> List.foldr([], fn
      [l], [] -> l
      [r, op], l -> {op, [l, r]}
    end)
  end
end
