defmodule AdventOfCode.Day04 do
  @valid_keys ~w(byr iyr eyr hgt hcl ecl pid cid)

  defmodule AdventOfCode.Day04.Passport do
    @enforce_keys [:byr, :iyr, :eyr, :hgt, :hcl, :ecl, :pid]
    defstruct [:byr, :iyr, :eyr, :hgt, :hcl, :ecl, :pid, :cid]
  end

  alias AdventOfCode.Day04.Passport

  def part1(args) do
    args |> parse_passports([], []) |> Enum.count()
  end

  def part2(args) do
    args |> parse_passports([], []) |> Stream.filter(&valid_passport?/1) |> Enum.count()
  end

  defp parse_passports([], _temp_collect, passports), do: passports

  # blank line reached, parse collected key:value strings and build a %Passport{}
  defp parse_passports(["" | rest], temp_collect, passports) do
    key_value_pairs =
      temp_collect
      |> Stream.map(&String.split(&1, ":", parts: 2))
      |> Stream.map(fn [key, value] -> {key, value} end)
      |> Stream.filter(fn {key, _value} -> key in @valid_keys end)
      |> Enum.map(fn {key, value} -> {String.to_atom(key), value} end)

    try do
      new_passport = struct!(Passport, key_value_pairs)
      parse_passports(rest, [], [new_passport | passports])
    rescue
      # not a valid passport
      ArgumentError -> parse_passports(rest, [], passports)
    end
  end

  # collect key:value strings from current line, and run next line
  defp parse_passports([line | rest], temp_collect, passports) do
    key_value_strings = String.split(line, " ")
    parse_passports(rest, key_value_strings ++ temp_collect, passports)
  end

  defp valid_passport?(%Passport{} = passport) do
    valid_byr?(passport) and valid_iyr?(passport) and valid_eyr?(passport) and
      valid_hgt?(passport) and valid_hcl?(passport) and valid_ecl?(passport) and
      valid_pid?(passport)
  end

  defp valid_byr?(%Passport{byr: byr}) do
    n = String.to_integer(byr)
    1920 <= n and n <= 2002
  end

  defp valid_iyr?(%Passport{iyr: iyr}) do
    n = String.to_integer(iyr)
    2010 <= n and n <= 2020
  end

  defp valid_eyr?(%Passport{eyr: eyr}) do
    n = String.to_integer(eyr)
    2020 <= n and n <= 2030
  end

  defp valid_hgt?(%Passport{hgt: hgt}) do
    suffix =
      cond do
        String.ends_with?(hgt, "cm") -> "cm"
        String.ends_with?(hgt, "in") -> "in"
        true -> ""
      end

    n = hgt |> String.replace_suffix(suffix, "") |> String.to_integer()

    case suffix do
      "cm" -> 150 <= n and n <= 193
      "in" -> 59 <= n and n <= 76
      "" -> false
    end
  end

  defp valid_hcl?(%Passport{hcl: "#" <> color}) do
    String.match?(color, ~r/^[a-f0-9]*$/) and String.length(color) == 6
  end

  defp valid_hcl?(_passport), do: false

  defp valid_ecl?(%Passport{ecl: ecl}) do
    ecl in ~w(amb blu brn gry grn hzl oth)
  end

  defp valid_pid?(%Passport{pid: pid}) do
    String.match?(pid, ~r/^[0-9]*$/) and String.length(pid) == 9
  end
end
