defmodule AdventOfCode.Day19 do
  def part1(args) do
    [rule_lines, message_lines] = String.split(args, "\n\n", parts: 2)
    messages = String.split(message_lines, "\n", trim: true)

    rules =
      rule_lines
      |> String.split("\n", trim: true)
      |> Enum.map(&parse_rule/1)
      |> Enum.into(%{})

    regex_str = build_regex_str(rules[0], rules)
    regex = ~r/^#{regex_str}$/

    Enum.count(messages, &is_valid_message?(&1, regex))
  end

  def part2(args) do
    [rule_lines, message_lines] = String.split(args, "\n\n", parts: 2)
    messages = String.split(message_lines, "\n", trim: true)

    rules =
      rule_lines
      |> String.split("\n", trim: true)
      |> Enum.map(&parse_rule/1)
      |> Enum.into(%{})

    regex_str = build_regex_str_part2(rules[0], rules)
    regex = ~r/^#{regex_str}$/

    Enum.count(messages, &is_valid_message?(&1, regex))
  end

  defp build_regex_str("a", _rules), do: "a"
  defp build_regex_str("b", _rules), do: "b"

  defp build_regex_str(rule, rules) do
    str =
      rule
      |> String.split("|")
      |> Enum.map(&String.split(&1, " ", trim: true))
      |> Enum.map(fn x -> Enum.map(x, &String.to_integer/1) end)
      |> Enum.map(fn numbers ->
        Enum.map(numbers, &build_regex_str(rules[&1], rules))
      end)
      |> Enum.join("|")

    "(#{str})"
  end

  defp build_regex_str_part2("a", _rules), do: "a"
  defp build_regex_str_part2("b", _rules), do: "b"

  defp build_regex_str_part2(rule, rules) do
    str =
      rule
      |> String.split("|")
      |> Enum.map(&String.split(&1, " ", trim: true))
      |> Enum.map(fn x -> Enum.map(x, &String.to_integer/1) end)
      |> Enum.map(fn numbers ->
        Enum.map(numbers, fn
          8 ->
            build_regex_str(rules[42], rules) <> "+"

          11 ->
            "(?'count'" <>
              build_regex_str(rules[42], rules) <>
              "(?&count)?" <>
              build_regex_str(rules[31], rules) <>
              ")"

          n ->
            build_regex_str(rules[n], rules)
        end)
      end)
      |> Enum.join("|")

    "(#{str})"
  end

  defp parse_rule(line) do
    [index_str, rule_str] = String.split(line, ": ", parts: 2)
    index = String.to_integer(index_str)
    rule = String.replace(rule_str, "\"", "")

    {index, rule}
  end

  defp is_valid_message?(message, regex) do
    Regex.match?(regex, message)
  end
end
