defmodule AdventOfCode.Day02 do
  def part1(args) do
    args |> Enum.map(&parse_line/1) |> Enum.count(&is_valid_password_part1?/1)
  end

  def part2(args) do
    args |> Enum.map(&parse_line/1) |> Enum.count(&is_valid_password_part2?/1)
  end

  defp parse_line(line) do
    [policy, password] = String.split(line, ": ", parts: 2)
    [min_max, letter] = String.split(policy, " ", parts: 2)
    [min_str, max_str] = String.split(min_max, "-", parts: 2)
    min = String.to_integer(min_str)
    max = String.to_integer(max_str)

    {min, max, letter, password}
  end

  defp is_valid_password_part1?({min, max, letter, password}) do
    count = password |> String.graphemes() |> Enum.count(fn x -> letter == x end)

    min <= count and count <= max
  end

  defp is_valid_password_part2?({first_pos, second_pos, letter, password}) do
    first = String.at(password, first_pos - 1) == letter
    second = String.at(password, second_pos - 1) == letter

    if first, do: not second, else: second
  end
end
