defmodule AdventOfCode.Day16 do
  def part1(args) do
    {rules, _my_ticket, nearby_tickets} = parse_input(args)

    nearby_tickets
    |> Enum.reduce({[], rules}, &collect_invalid_values/2)
    # select only resulting acc
    |> elem(0)
    |> List.flatten()
    |> Enum.sum()
  end

  def part2(args) do
    {rules, my_ticket, nearby_tickets} = parse_input(args)

    columns =
      nearby_tickets
      |> Enum.filter(&is_valid_ticket?(&1, rules))
      |> tickets_to_columns()

    relevant_indices =
      columns
      |> Enum.with_index()
      |> Enum.reduce({[], rules}, &find_rules_for_colum/2)
      # select only resulting acc
      |> elem(0)
      |> Enum.sort_by(fn {_index, rules} -> length(rules) end)
      |> Enum.reduce(%{}, &remove_duplicates/2)
      |> Enum.filter(fn {name, _index} -> String.contains?(name, "departure") end)
      # select only indices
      |> Enum.map(&elem(&1, 1))

    relevant_indices
    |> Enum.map(&Enum.at(my_ticket, &1))
    |> Enum.reduce(&Kernel.*/2)
  end

  defp parse_input(args) do
    [rules_str, "your ticket:\n" <> my_ticket_str, "nearby tickets:\n" <> nearby_tickets_str] =
      String.split(args, "\n\n", parts: 3)

    rules = rules_str |> String.split("\n", trim: true) |> Enum.map(&parse_rule/1)

    my_ticket = my_ticket_str |> String.trim() |> parse_ticket()

    nearby_tickets =
      nearby_tickets_str |> String.split("\n", trim: true) |> Enum.map(&parse_ticket/1)

    {rules, my_ticket, nearby_tickets}
  end

  defp parse_rule(line) do
    [name, ranges_str] = String.split(line, ": ", parts: 2)

    ranges =
      ranges_str
      |> String.split(" or ")
      |> Enum.map(&String.split(&1, "-", parts: 2))
      |> Enum.map(fn [s, e] -> String.to_integer(s)..String.to_integer(e) end)

    {name, ranges}
  end

  defp parse_ticket(line), do: line |> String.split(",") |> Enum.map(&String.to_integer/1)

  defp collect_invalid_values(ticket, {acc, rules}) do
    case Enum.filter(ticket, &is_invalid_value?(&1, rules)) do
      [] -> {acc, rules}
      found -> {[found | acc], rules}
    end
  end

  defp is_invalid_value?(n, rules) do
    rules
    # select only rule ranges
    |> Enum.flat_map(&elem(&1, 1))
    |> Enum.all?(fn range -> n not in range end)
  end

  defp is_valid_ticket?(ticket, rules) do
    Enum.all?(ticket, fn n -> not is_invalid_value?(n, rules) end)
  end

  defp tickets_to_columns([t | _] = tickets) do
    for i <- 1..length(t), do: Enum.map(tickets, &Enum.at(&1, i - 1))
  end

  defp find_rules_for_colum({column, index}, {acc, rules}) do
    matching_rule_names =
      rules
      |> Enum.filter(fn rule -> is_valid_ticket?(column, [rule]) end)
      # select only rule name
      |> Enum.map(&elem(&1, 0))

    next_acc = [{index, matching_rule_names} | acc]

    {next_acc, rules}
  end

  defp remove_duplicates({index, rules}, acc) do
    case rules |> Enum.filter(fn rule -> Map.get(acc, rule) == nil end) do
      [single] -> Map.put(acc, single, index)
    end
  end
end
